# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 15:21:04 2017

@author: Lime
"""

import random, math, pylab

sigma = 1.0
mu = 0.0

def gaussian(x):
    res = math.exp (- (x - mu) * (x - mu) /(2.0 * sigma * sigma)) / math.sqrt(2 * math.pi * sigma * sigma)
    return res


def mcmc(N, x0, delta, data):
    x = x0
    for i in range(N):
        x_new = x + random.uniform(-delta, delta)       
        if random.uniform(0.0, 1.0) <  gaussian(x_new) / gaussian(x): 
            x = x_new 
        data.append(x)

def U(x):
    return ((x - mu) * (x - mu)) / (2.0 * sigma * sigma) - math.log(1 / math.sqrt(2 * math.pi * sigma * sigma))

def dU(x):
    return (-(x - mu) / ((sigma ** 3) * math.sqrt(2 * math.pi))) * math.exp((-(x - mu) * (x - mu)) / (2.0 * sigma * sigma))

def hmc(N, L, x0, delta, data):
    x = x0
    for i in range(N):
        p = random.gauss(0.0, 1.0)
        
        #######################
        #Leap frog algorithm
        p_new = p - (delta / 2.0) * dU(x)
        x_new = x + delta * p_new
        
        for l in range(L):
            p_new = p_new - delta * dU(x_new)
            x_new = x_new + delta * p_new
        
        p_new = p_new - (delta / 2.0) * dU(x_new)
        #######################
        U0 = U(x)
        U_new = U(x_new)
        K0 = (p * p) / 2.0
        K_new = (p_new * p_new) / 2.0
        
        alpha = min(1.0, math.exp(U0 + K0 - U_new - K_new))
        
        q = random.uniform(0.0, 1.0)
        if q < alpha:
            x = x_new
        
        data.append(x)


delta1 = 0.03
data1 = []
N1 = 2048
L = 25
x0 = 15.0
hmc(N1, L, x0, delta1, data1)

delta2 = 0.5
data2 = []
N2 = 2048
mcmc(N2, x0, delta2, data2)



pylab.hist(data1, 100, normed = 'True')
x = [a / 10.0 for a in range(-100, 100)]
y = [gaussian(a) for a in x]

pylab.plot(x, y, c='red', linewidth=2.0)
pylab.title('Gaussian $\pi({}, {})$ and normalized histogram \n for '.format(mu, sigma) +str(len(data1))+' HMC samples', fontsize = 14)
pylab.xlabel('$x$', fontsize = 14)
pylab.ylabel('$\pi(x)$', fontsize = 14)
pylab.savefig('plot_hmc_gauss.png')
pylab.show()


pylab.hist(data2, 100, normed = 'True')

pylab.plot(x, y, c='red', linewidth=2.0)
pylab.title('Gaussian $\pi({}, {})$ and normalized histogram \n for '.format(mu, sigma) +str(len(data2))+' MCMC samples', fontsize = 14)
pylab.xlabel('$x$', fontsize = 14)
pylab.ylabel('$\pi(x)$', fontsize = 14)
pylab.savefig('plot_mcmc_gauss.png')
pylab.show()
